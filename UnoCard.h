#pragma once
#include <string>
#include <iostream>
class UnoCard {
public:
	int get_val() const {return _val; }
	std::string get_color() const { return _color; }
	UnoCard(int val, std::string color) { _val = val; _color = color; } //std::cout << "ok" << std::endl; 
	UnoCard(UnoCard *x) { _val = x->_val; _color = x->_color; }
	UnoCard() { /*std::cout << "YOU DIDNT PASS VALUES - UNOCARD CONSTRUCTOR" << std::endl; */}
	~UnoCard() {}
	friend std::ostream& operator<<(std::ostream& os, const UnoCard &c);
	friend bool operator<(UnoCard a, UnoCard b) {
		if (a.get_val() < b.get_val()) { return true; }
		else return false;
	}

private:
	std::string _color;
	int _val;
};

std::ostream& operator<<(std::ostream& os, const UnoCard &c) {
	os << c.get_color() << " " << c.get_val();
	return os;
}