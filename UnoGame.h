#pragma once
#pragma warning(disable : 4996)	// THIS IS FROM THE WEIRD ERROR- SAID TO DO THIS< PLEASE DONT DIE LAPTOP
#include "Stack.h"
#include"Queue.h"
#include"Bag.h"
#include "UnoCard.h"
#include "UnoPlayer.h"
#include<iostream>
#define NUM_OF_PLAYERS 3
#define NUM_OF_CARDS_PER_PLAYER 7
#define NUM_OF_NUMS_FOR_CARDS 5
#define NUM_OF_COLORS 4

class UnoGame {
public:
	UnoGame();
	void drawCard(UnoPlayer pl);	// SHOULD THIS BE here/ need corresponding in player to adjust hand aswell
//	void playCard();	/// DO WE NEED THIS>
	void shuffleDeck();
	void startDeck();
	void takeTurn(UnoPlayer pl);


private:
	int _numberOfPlayers;
	int _playerHandCap;
	int _maxCardVal;
	Stack<UnoCard> _faceUpDeck;
	Queue<UnoCard> _playDeck;
	Queue<UnoPlayer> _playlist;
};

UnoGame::UnoGame() {
	_maxCardVal = NUM_OF_NUMS_FOR_CARDS;
	_playerHandCap = NUM_OF_CARDS_PER_PLAYER;
	_numberOfPlayers = NUM_OF_PLAYERS;

	startDeck();

	for (int i = 0; i < _numberOfPlayers; i++) {
		std::string name;
		std::cout << "Please enter a player name: ";
		std::cin >> name;
		UnoPlayer* player = new UnoPlayer(name);
		/*Queue<int> _test;
		_test.enqueue(5);*/
		//UnoPlayer bill("bill");
		std::cout << "GOT HERE" << std::endl;
		_playlist.enqueue(*player);//&
	}
	
	for (int k = 0; (k < _playerHandCap) && (_playDeck.size() != 0);k++) {	// deals hands
		for (int o = 0; o < _numberOfPlayers; o++) {
			UnoPlayer temp = _playlist.dequeue();
			std::cout << "GOT HERE" << std::endl;

			temp.pickUpCard(_playDeck.dequeue());
			std::cout << "GOT HERE" << std::endl;

			_playlist.enqueue(temp); //&
		}
	}

	std::cout << "GOT HERE" << std::endl;

	if (_playDeck.size() != 0) {
		_faceUpDeck.push(_playDeck.dequeue());
	}//WHY THIS LINE
	std::cout << "The deck has been created, first face-up card is: " << std::endl << _faceUpDeck.peek() << std::endl;

	std::cout << "HERE WE ARE" <<_playlist.peek()<< std::endl;
	bool wonvar = false;
	for (UnoPlayer cpl = _playlist.dequeue(); wonvar != false; cpl = _playlist.dequeue()) {
		if (_playDeck.size() == 0) {shuffleDeck(); }
		takeTurn(cpl);		
		wonvar = cpl.playerWon();
		_playlist.enqueue(cpl);//
	}
}


inline void UnoGame::shuffleDeck(){
	UnoCard topcard = _faceUpDeck.pop();
	Bag<UnoCard> shufflebag;
	while (!(_faceUpDeck.isEmpty())) {
		shufflebag.add(_faceUpDeck.pop());
	}
	while (!(shufflebag.isEmpty())) {
		_playDeck.enqueue(shufflebag.getOne());
	}
	if (_faceUpDeck.isEmpty()) { _faceUpDeck.push(topcard); }
	else { std::cout << "SOMETHING WENT WRONG IN SHUFFLE" << std::endl; }
}

inline void UnoGame::startDeck() {
	Bag<UnoCard> _baggedDeck(40);
	for (int k = 1; k <= _maxCardVal; k++) {
		for (int i = 0; i < NUM_OF_COLORS; i++) {
			if (i == 0) {
				_baggedDeck.add(UnoCard(k, "Red"));
				_baggedDeck.add(UnoCard(k, "Red"));
				//std::cout << "RED ADDED" << std::endl;
			}
			else if (i == 1) {
				_baggedDeck.add(UnoCard(k, "Blue"));
				_baggedDeck.add(UnoCard(k, "Blue"));
			}
			else if (i == 2) {
				_baggedDeck.add(UnoCard(k, "Yellow"));
				_baggedDeck.add(UnoCard(k, "Yellow"));
			}
			else if (i == 3) {
				_baggedDeck.add(UnoCard(k, "Green"));
				_baggedDeck.add(UnoCard(k, "Green"));
			}
		}
	}
	for (int j = 0; j < (_maxCardVal * 8); j++) {
		_playDeck.enqueue(_baggedDeck.getOne());
	}
	
}
void UnoGame::takeTurn(UnoPlayer pl) {
	UnoCard upcard = _faceUpDeck.peek();
	if (int match= pl.isMatch(upcard) > 0) {
		UnoCard x= pl.playCard(match);
		_faceUpDeck.push(x);
	}
	else {
		if (_playDeck.size() != 0){
			pl.pickUpCard(_playDeck.dequeue());
			pl.playerUno();
		}
		else if (_playDeck.size() == 0) {

		}
		else { std::cout << "SOMETHING HAS GONE HORRIBLY WRONG IN TAKETURN" << std::endl; }
	}
}
void UnoGame::drawCard(UnoPlayer pl) {
	UnoCard c = _playDeck.dequeue();
	pl.pickUpCard(c);
	std::cout << pl << " picked up a " << c << std::endl;
}
