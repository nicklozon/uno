#pragma once
#include <string>
#include <iostream>
#include "OrderedList.h"				// okay so i just made the _hand  all (*_hand) and declared it as *_hand
#include "UnoCard.h"
class UnoPlayer {
public:
	int isMatch(UnoCard x);	// returns the value at the location > DELETES the actual spot using remove
	UnoPlayer() { std::cout << "PLEASE GIVE ME THINGS - CONSTRUCTOR OF UNOPLAYER" << std::endl; }
	UnoPlayer(std::string name) { _name = name; std::cout << "Player: " << _name << " has joined the game!" << std::endl; }
	UnoPlayer(UnoPlayer *pl) { _name = (*pl).get_name(); _hand = pl->_hand; std::cout << "IN NAME PLAYER COPY CON" << std::endl; }
	~UnoPlayer() {}
	UnoCard playCard(int i);

	bool playerWon();
	void playerUno();
	void pickUpCard(UnoCard card);
	std::string get_name() const { return _name; }
	void set_name(std::string name) { _name = name; }

	friend std::ostream& operator<<(std::ostream& os, const UnoPlayer &pl) {
		os << pl.get_name();
		return os;
	}

private:
	std::string _name;
	OrderedList< UnoCard > _hand;
	
	int colormatch(std::string fm);
	int valmatch(int fm);
};
void UnoPlayer::pickUpCard( UnoCard card) {//CONSIDER CONST HERE
	_hand.insert(card);
}
UnoCard UnoPlayer::playCard( int i) {// DONT KNOW THAT WE NEED THIS FUNCTION
	UnoCard x= _hand.remove(i);
	std::cout << x << std::endl;	// ADD IT TO THE FACE UP PILE FROM GAME
	return x;								// The card should be removed in the game, so pass jim.playcard(_hand.remove(i)) OR SOMETHING. THINK HERE
}

inline bool UnoPlayer::playerWon() {
	if (_hand.getLength() == 0) {
		return true;
	}
	return false;
}
inline void UnoPlayer::playerUno(){
	if (_hand.getLength() == 1) {
		std::cout<<_name<<" has UNO"<<std::endl;
	}
}
int UnoPlayer::colormatch(std::string fm) {
	for (int i = 1; i <= _hand.getLength(); i++) {
		if (_hand.retrieve(i).get_color() == fm) {
			return i;  // _hand.remove(i); - this was for returning the Uno card - now have th game then remove(i) instead
		}
	}
	return -1;
}
int UnoPlayer::isMatch(UnoCard x) {
	if (int p=valmatch(x.get_val()) > 0) {
		return p;
	}
	else if (int q = colormatch(x.get_color()) > 0) {
		return q;
	}
	else return -1;
}
int UnoPlayer::valmatch(int fm) {
	for (int i = 1; i <= _hand.getLength(); i++) {	// indexed at one to account for length starting at 1
		if (_hand.retrieve(i).get_val() == fm) {
			return i; // _hand.remove(i);
		}
	}
	return -1;
}